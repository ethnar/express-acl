const SSL = false;
const SECRET = 'whatever';

const express = require('express');
const path = require('path');
const fs = require('fs');
const https = require('https');

const port = process.env.PORT || 8443;

const expressApp = express();

const sessionTokens = {};

const getSessionFromCookies = (cookiesString) => {
    const cookies = cookiesString
        .split(';')
        .map(item => item
            .trim()
            .split('=')
        )
        .reduce((acc, [key, value]) => Object.assign(acc, { [key]: value }), {});
    const token = cookies.authToken;
    return sessionTokens[token];
};

const sendLoginPage = (res) => {
    res.sendFile(path.join(__dirname, './login.html'));
};

let serverApp;
if (SSL) {
    serverApp = https.createServer({
        key: fs.readFileSync('./ssl/privatekey.pem'),
        cert: fs.readFileSync('./ssl/certificate.pem'),
    }, expressApp);
} else {
    serverApp = expressApp;
}

expressApp
    .use(express.static('login'))
    .post('/api/login', (req, res) => {
        let bodyStr = '';
        req.on('data',function(chunk){
            bodyStr += chunk.toString();
        });
        req.on('end',function(){
            // TODO: should target the params better
            if (bodyStr.split('=')[1] === SECRET) {
                // TODO: would be nice to improve token generation
                const token = Math.random() + '_' + new Date().getTime();
                sessionTokens[token] = {};
                res.cookie('authToken', token, { maxAge: 100000000000, httpOnly: true });
                res.redirect(301, '/');
                res.send();
            } else {
                sendLoginPage(res);
            }
        });
    })
    .use('/', (req, res) => {
        const session = getSessionFromCookies(req.headers.cookie);
        if (!session) {
            sendLoginPage(res);
            return;
        }
        res.sendFile(path.join(__dirname, './gallery/' + req.path));
    });

serverApp
    .listen(port);

console.log('Server listening on port', port);
